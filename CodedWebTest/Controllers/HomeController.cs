﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using CodedWebTest.Data;
using Microsoft.AspNetCore.Mvc;
using CodedWebTest.Entities;
using CodedWebTest.Services;
using System.Globalization;
using Microsoft.AspNetCore.Http;

namespace CodedWebTest.Controllers
{
    public class RequestModel
    {
        public string email { get; set; }
    }

    public class HomeController : Controller
    {
        private readonly WebTestDBContext _ctx;
        private readonly ISessionDataService _sessionData;

        public HomeController(WebTestDBContext ctx, ISessionDataService sessionData)
        {
            // TODO: Get WebTestDBContext via Dependency Injection
            // TODO: Get SessionData via Dependency Injection
            _ctx = ctx;
            _sessionData = sessionData;
        }

        public IActionResult Index()
        {
            return View();
        }

        // TODO: Check if email exists in the database
        [HttpPost("checkEmail")]
        public async Task<IActionResult> CheckEmail([FromBody] RequestModel request)
        {
            try
            {
                var result = _ctx.EmailAddress.Any(e => e.Address == request.email);
                var resp = (result == true) ? "Email exists in the database" : "Email not found in the database";

                return new JsonResult(new { result = "ok", value = resp });
            }
            catch (Exception e)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

        // TODO: Save email to the database and user session
        [HttpPost("saveEmail")]
        public async Task<IActionResult> SaveEmail([FromBody] RequestModel request)
        {
            try
            {
                _ctx.EmailAddress.Add(new EmailAddress { EmailAddressUid = Guid.NewGuid(), Address = request.email, CreatedDate = DateTime.ParseExact("09/01/2021", "MM/dd/yyyy", CultureInfo.InvariantCulture) });
                await _ctx.SaveChangesAsync();

                _sessionData.EmailAddress = request.email;

                return new JsonResult(new { result = "ok", value = "Email processed" });
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

        // TODO: Get saved email from session
        [HttpGet("getSessionEmail")]
        public async Task<IActionResult> GetEmail()
        {
            try
            {
                string response = "Saved email from session: " + _sessionData.EmailAddress;
                return new JsonResult(new { result = "ok", value = response });
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }
    }
}
